#include "StackAllocator.h"

#define MB 1024*1024

StackAllocator* StackAllocator::c_stackAllocator = nullptr;

StackAllocator::StackAllocator(U32 stackSize_bytes)
{

	this->marker = (Marker)malloc(stackSize_bytes);
	this->currMarker = this->marker;

}

void* StackAllocator::alloc(U32 size_bytes)
{

	Marker temp = this->currMarker;

	this->currMarker += size_bytes;
	
	return (void*)temp;
}

StackAllocator::Marker StackAllocator::getMarker()
{
	return this->currMarker;
}

void StackAllocator::freeToMarker(Marker marker)
{

	this->currMarker = marker;
}

void StackAllocator::clear()
{
	this->currMarker = this->marker;
}

StackAllocator::~StackAllocator()
{

	free((void*)this->marker);
}

StackAllocator& StackAllocator::getInstance() {

	if (c_stackAllocator == nullptr) {

		c_stackAllocator = new StackAllocator(MB * 500);
	}

	return *c_stackAllocator;
}

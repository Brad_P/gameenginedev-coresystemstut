#include "GameObjectFactory.hpp"

#include "NPC.hpp"
#include "Rocket.hpp"

#include <cassert>
#include <cstring>

GameObjectFactory::GameObjectFactory()
{

}

GameObjectFactory::~GameObjectFactory()
{

}

/* static */ 
GameObject* GameObjectFactory::create(const char* pType)
{
    /* The book uses strcmpi here, this appears to be some kind of windows only 
       method which I'm avoiding. Strcasecmp is POSIX and BSD standard from 2001 
    */
    if(!strcmpi(pType,"npc"))
    {
        return new NPC();
    }
    else
    {        
        if(!strcmpi(pType,"rocket"))
        {
            return new Rocket();
        }
        else
        {
            assert(!"GameObjectFactory::Create(): unknown Type");
        }
    }
}

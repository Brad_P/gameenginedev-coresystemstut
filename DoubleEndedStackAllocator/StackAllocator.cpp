#include "StackAllocator.h"

#define MB 1024*1024

StackAllocator* StackAllocator::c_stackAllocator = nullptr;

StackAllocator::StackAllocator(U32 stackSize_bytes)
{

	this->topMarker = (Marker)malloc(stackSize_bytes);
	this->bottomMarker = (Marker)malloc(stackSize_bytes);

	this->currTopMarker = this->topMarker;
	this->currBottomMarker = this->bottomMarker;

}

void* StackAllocator::allocTop(U32 size_bytes)
{

	Marker temp = this->currTopMarker;

	this->currTopMarker += size_bytes;
	
	return (void*)temp;
}

void* StackAllocator::allocBottom(U32 size_bytes) {

	Marker temp = this->currBottomMarker;

	this->currBottomMarker -= size_bytes;

	return (void*)temp;
}

StackAllocator::Marker StackAllocator::getTopMarker()
{
	return this->currTopMarker;
}

StackAllocator::Marker StackAllocator::getBottomMarker() {

	return this->currBottomMarker;
}

void StackAllocator::freeToTopMarker(Marker marker)
{

	this->currTopMarker = marker;
}

void StackAllocator::freeToBottomMarker(Marker marker) {

	this->currBottomMarker = marker;
}

void StackAllocator::clearTop()
{
	this->currTopMarker = this->topMarker;
}

void StackAllocator::clearBottom() {

	this->currBottomMarker = this->bottomMarker;
}


StackAllocator::~StackAllocator()
{
	free((void*)this->topMarker);
	free((void*)this->bottomMarker);
}

StackAllocator& StackAllocator::getInstance() {

	if (c_stackAllocator == nullptr) {

		c_stackAllocator = new StackAllocator(MB * 500);
	}

	return *c_stackAllocator;
}

#include <cstdlib>
#include <cstdint>

class StackAllocator
{

public:
	//Not part of Gregory's interface (but implied). 
	typedef intptr_t U32;
	typedef intptr_t Marker;

	//Explicit keyword ensures this is not used
        //as a conversion constructor. 
	explicit StackAllocator(U32 stackSize_bytes);

	virtual ~StackAllocator();

	//Allocate a block (return start). 
	void* allocTop(U32 size_bytes);
	void* allocBottom(U32 size_bytes);
	
	//Get stack top (a marker). 
	Marker getTopMarker();
	Marker getBottomMarker();


	//Unroll the stack to the marker given. 
	void freeToTopMarker(Marker marker);
	void freeToBottomMarker(Marker marker);


	//Unroll the whole stack. 
	void clearTop();
	void clearBottom();

	static StackAllocator& getInstance();

private:
	Marker topMarker;
	Marker bottomMarker;
	Marker currTopMarker;
	Marker currBottomMarker;

	static StackAllocator* c_stackAllocator;

};

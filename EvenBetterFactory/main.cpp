/* Better Factory

Creating a Better Factory to test the ideas in Object-Orientated Games Development by Julian Gold.  This is a better version of the first, Gold suggests that removing the coupling with respect to the enum in the object class makes things cleaner, i.e. its better if you can just use a string to name the kind of object you'd like to create.

To compile:  Use premake4 to configure the project as in the premake4.lua file, build files for a range of environments can be created (see premake4 --help)

*/

#include <iostream>
#include "GameObjectFactory.hpp"
#include "Rocket.hpp"
#include "NPC.hpp"

int main(int argc, char* argv[])
{
    std::cout << "Hello World" << std::endl;

    GameObject *one = GameObjectFactory::getInstance().create("NPC");
    GameObject *two = GameObjectFactory::getInstance().create("Rocket");

    one->draw();
    two->draw();

    delete one;
    delete two;

    //force an error.
	/*
    GameObject *three = GameObjectFactory::getInstance().create("Fish");
    if(three != 0)
    {
        three->draw();
        delete three;
    }
    else
        std::cout << "Got a null ptr from the factory, is there a better way to handle this?" << std::endl;
	*/
}


/* Based on C Programming - K&R
*  Badly ported by GlJ
*/

#ifndef STRING_HASHER_HPP
#define STRING_HASHER_HPP

#include <string>


typedef struct nlist { /* table entry: */
    struct nlist *next; /* next entry in chain */
    std::string name; /* defined name */
} nlist;


using std::string;

class StringHasher
{
    private:
			//Note the Constructor is private, i.e. Singleton
	    StringHasher();
	    ~StringHasher();

  		static const int HASHSIZE = 101;
			nlist *hashtab[HASHSIZE]; /* pointer table */

	    static StringHasher hasher;

    public:
	    static StringHasher& getInstance();

			unsigned hash(string s);

			nlist* lookup(string s);
			nlist* install(string name);
};

#endif

/* Simple Factory

Creating a Factory to test the ideas in Object-Orientated Games Development by Julian Gold.  This is the most simple type of factory, it has some issues.  The Factory is close coupled to everything that uses it, i.e. changing the enum causes problems.  Gold removes each of these barriers in turn, I'm going to create and updated version of this project to do the same working up to the more advanced version.  I may even look at the templated one.

To compile:  Use premake4 to configure the project as in the premake4.lua file, build files for a range of environments can be created (see premake4 --help)

*/

#include <iostream>
#include "GameObjectFactory.hpp"

int main(int argc, char* argv[])
{
    std::cout << "Hello World" << std::endl;

    GameObject *one = GameObjectFactory::create(GameObject::Type::NPC);
    GameObject *two = GameObjectFactory::create(GameObject::Type::ROCKET);

    one->draw();
    two->draw();

    delete one;
    delete two;

    //force an error.
    //GameObject *three = GameObjectFactory::create(GameObject::Type::NUM_TYPES);
    //three->draw();
    //delete three;
}

